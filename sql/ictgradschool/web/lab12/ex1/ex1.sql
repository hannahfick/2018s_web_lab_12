#     A. Display the departments offering courses

SELECT DISTINCT dept
FROM unidb_attend;

#     B. Display the semesters being attended

SELECT DISTINCT semester
FROM unidb_attend;

#     C. Display the courses that are attended

SELECT DISTINCT num, descrip
FROM unidb_courses;

#     D. List the student names and country, ordered by first name
SELECT fname, lname, country
FROM unidb_students
ORDER BY fname;

#     E. List the student names and mentors, ordered by mentors
SELECT mentor.fname, mentor.lname, student.fname, student.lname
FROM unidb_students AS mentor,
     unidb_students as student
WHERE mentor.id = student.mentor
ORDER BY mentor.lname;

#     F. List the lecturers, ordered by office
SELECT CONCAT(fname, ' ', lname), office
FROM unidb_lecturers
ORDER BY office;

#     G. List the staff whose staff number is greater than 500
SELECT CONCAT(fname, ' ', lname)
FROM unidb_lecturers
WHERE staff_no > 500;

#     H. List the students whose id is greater than 1668 and less than 1824
SELECT CONCAT(fname, ' ', lname)
FROM unidb_students
WHERE id BETWEEN 1667 AND 1824;

#     I. List the students from NZ, Australia and US

SELECT CONCAT(fname, ' ', lname)
FROM unidb_students AS students
WHERE students.country = 'NZ'
   OR students.country = 'US'
   OR students.country = 'AU';

#     J. List the lecturers in G Block
SELECT CONCAT(fname, ' ', lname)
FROM unidb_lecturers
WHERE office LIKE 'G%';

#     K. List the courses not from the Computer Science Department
SELECT DISTINCT descrip
FROM unidb_courses
WHERE dept NOT LIKE 'comp';

#     L. List the students from France or Mexico

SELECT fname, lname
FROM unidb_students
WHERE country = 'FR'
  OR country = 'MX';