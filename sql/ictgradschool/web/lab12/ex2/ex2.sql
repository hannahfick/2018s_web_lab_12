-- Answers to exercise 2 questions

# A. What are the names of the students who attend COMP219?

SELECT students.fname, students.lname
FROM unidb_students AS students,
     unidb_attend AS courses
WHERE students.id = courses.id
  AND courses.num = '219'
  AND courses.dept = 'comp';

# B. What are the names of the student reps that are not from NZ?

SELECT student.fname, student.lname, student.id
FROM unidb_courses AS reps,
     unidb_students AS student
WHERE student.country != 'NZ'
  AND reps.rep_id = student.id;

# C. Where are the offices for the lecturers of 219?

SELECT office
FROM unidb_lecturers AS lecturers,
     unidb_teach AS coursetaught
WHERE lecturers.staff_no = coursetaught.staff_no
  AND coursetaught.num = '219'
  AND coursetaught.dept = 'comp';

# D. What are the names of the students taught by Te Taka?

SELECT DISTINCT students.fname, students.lname
FROM unidb_attend AS courses,
     unidb_teach AS taught,
     unidb_lecturers AS lecturer,
     unidb_students AS students
WHERE lecturer.fname = 'Te Taka'
  AND lecturer.staff_no = taught.staff_no
  AND taught.num = courses.num
  AND taught.dept = courses.dept
  AND courses.id = students.id;

# E. List the students and their mentors

SELECT student.fname, student.lname, mentor.fname, mentor.lname
FROM unidb_students AS mentor,
     unidb_students as student
WHERE mentor.id = student.mentor;

# F. Name the lecturers whose office is in G-Block as well naming the students that are not from NZ

SELECT fname, lname
FROM unidb_lecturers
WHERE office LIKE 'G%'
UNION
SELECT fname, lname
FROM unidb_students
WHERE country != 'NZ';

# G. List the course coordinator and student rep for COMP219

SELECT rep.fname, rep.lname
FROM unidb_students as rep,
     unidb_courses as course
WHERE course.dept = 'comp'
  AND course.num = '219'
  AND course.rep_id = rep.id
UNION
SELECT coordinator.fname, coordinator.lname
FROM unidb_lecturers as coordinator,
     unidb_courses AS courses
WHERE courses.coord_no = coordinator.staff_no
  AND courses.dept = 'comp'
  ANd courses.num = '219';


# not a union way below. better example above.

SELECT rep.fname, rep.lname, coordinator.fname, coordinator.lname
FROM unidb_courses AS courses,
     unidb_lecturers AS coordinator,
     unidb_students AS rep
WHERE courses.rep_id = rep.id
  AND coordinator.staff_no = courses.coord_no
  AND courses.dept = 'comp'
  AND courses.num = '219';