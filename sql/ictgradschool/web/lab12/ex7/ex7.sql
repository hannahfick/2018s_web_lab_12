ALTER TABLE team
  DROP FOREIGN KEY FK_leagueName;
ALTER TABLE playerteam
  DROP FOREIGN KEY FK_playerID;
ALTER TABLE playerteam
  DROP FOREIGN KEY FK_teamname;

DROP TABLE IF EXISTS league;
DROP TABLE IF EXISTS player;
DROP TABLE IF EXISTS team;
DROP TABLE IF EXISTS playerteam;

CREATE TABLE league (
  leagueName VARCHAR(32),
  PRIMARY KEY (leagueName)
);

CREATE TABLE player (
  playerID    INT(20),
  age         INT(2),
  nationality VARCHAR(20),
  fname       VARCHAR(30),
  lname       VARCHAR(30),
   PRIMARY KEY (playerID)

);


CREATE TABLE team (
  teamname   VARCHAR(20),
  homecity   VARCHAR(20),
  points     INT(3),
  captain    INT(20),
  leagueName VARCHAR(32),
  PRIMARY KEY (teamname),
  CONSTRAINT FK_leaguename FOREIGN KEY (leagueName) REFERENCES league (leaguename)
);

CREATE TABLE playerteam (

  id       INT,
  playerID INT(20),
  teamname VARCHAR(20),
  PRIMARY KEY (id),
  CONSTRAINT FK_playerID FOREIGN KEY (playerID) REFERENCES player(playerID),
  CONSTRAINT FK_teamname FOREIGN KEY (teamname) REFERENCES team (teamname)

);

INSERT INTO league (leagueName)
VALUES ('BabyLeague');


INSERT INTO player (playerID, age, nationality, fname, lname)
VALUES (1661, 21, 'US', 'Bobby', 'Bobster'),
       (1662, 21, 'US', 'Sammy', 'Samster'),
       (1663, 21, 'US', 'Larry', 'Larster'),
       (1664, 21, 'AU', 'Jimmy', 'Jimson'),
       (1665, 21, 'AU', 'Barty', 'Bartson'),
       (1666, 21, 'AU', 'Shauny', 'Shaunston'),
       (1667, 21, 'NZ', 'Yaz', 'Yazzzzzz'),
       (1668, 21, 'NZ', 'Darcy', 'Dude'),
       (1669, 21, 'NZ', 'Steph', 'Stephiecakes');

INSERT INTO team (teamname, homecity, points, captain, leagueName)
VALUES ('Bobcats', 'New York', 5, 1, 'BabyLeague'),
       ('Sallies', 'Sydney', 100, 2, 'BabyLeague'),
       ('NZers', 'Huntly', 200, 2, 'BabyLeague');

